#!/bin/bash -e

# Copyright 2017 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [[ -e /customize_for_vm_type.sh ]]; then
  /customize_for_vm_type.sh
fi

# Start fresh to isolate user-initiated actions from VM build & startup events.
for f in /var/log/apt/history.log /var/log/pip.log; do
  mv "$f" "${f}.bak-run.sh" || true  # Ignore missing files.
done

# Translate SIGTERM upon docker stop to SIGKILL in order to facilitate an
# orderly shutdown. We use SIGKILL since we don't need to wait for in-flight
# requests to conclude.
terminate() {
  echo "run.sh: terminate trap handler: node PID: ${NODE_PID}"
  kill -KILL "${NODE_PID}"
}
trap terminate SIGTERM

cd /

# Start the node server.
/tools/node/bin/node /datalab/web/app.js &
NODE_PID=$!
wait "${NODE_PID}"
